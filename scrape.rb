# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'
require 'pry'

class HopScraper
  def initialize(mode: :update)
    @mode = mode # update or refresh
    @hop_list_url = 'http://www.hopslist.com/hops/'
  end

  attr_reader :mode, :hop_list_url, :hops

  def perform
    puts "Parsing #{hop_list_url}"
    doc = Nokogiri::HTML(URI.open(hop_list_url))
    hop_urls = doc.css(".listing-item").map { |li| li.at_css('.title')['href'] }

    puts "Found #{hop_urls.size} urls:"
    puts hop_urls

    @hops = hop_urls.map { |url| parse_hop(url) }

    puts "Fetched #{hops.size} hops"
    puts hops
  end

  def parse_hop(url)
    doc = Nokogiri::HTML(URI.open(url))

    hop_attribute_table = doc.css('.entry-content tbody')
    advertisement, content = hop_attribute_table

    hop_node_to_use = content || advertisement

    hop = Hop.new

    hop_node_to_use.css('tr').each do |hop_attribute|
      attribute, value = hop_attribute.css('td')
      next if value&.text.nil? || value.text.length == 0
      hop.send("#{attribute.text.downcase.gsub(/\W/, '_')}=", value.text)
    end

    hop.name = doc.at_css('.entry-title').text

    puts "Parsed hop #{hop.name}!"

    hop
  end
end

class Hop < OpenStruct

end


HopScraper.new.perform
